import shutil

class Binary:
    def __init__(self, session, json_data):
        self._session = session
        self.file_name = json_data['fileName']
        self._thumbnail_url = json_data['thumbnailLink']
        self.size = json_data['sizeInBytes']
        self._url = json_data['binaryLink']

    def download(self):
        return self._session.get(self._url, stream=True)

    def download_to_file(self, path):
        self._url_to_file(self._url, path)

    def download_thumbnail(self):
        return self._session.get(self._thumbnail_url, stream=True)

    def download_thumbnail_to_file(self, path):
        self._url_to_file(self._thumbnail_url, path)

    def _url_to_file(self, url, path):
        resp = self._session.get(url, stream=True)
        with open(path, 'wb') as f:
            resp.raise_for_status()
            shutil.copyfileobj(resp.raw, f)

    def __str__(self):
        return '<binary name={}>'.format(self.file_name)
