from datetime import datetime, timedelta, timezone
import functools
import requests
import tocco as _tocco
import uuid

class Records:
    def __init__(self, session, entity, url, params):
        self._session = session
        self._entity = entity
        self._url = url
        self._params = params

    def __repr__(self):
        return '<records entity={}>'.format(self._entity)

    def __iter__(self):
        return _RecordsIter(self)


class _RecordsIter:
    def __init__(self, records):
        self._records = records
        self._next_page = None
        self._has_next_page = True
        self._page = iter([])

    def __repr__(self):
        return '<records iter entity={}>'.format(self._records._entity)

    def __next__(self):
        next_item = next(self._page, None)
        if next_item is not None:
            return Record(self._records._session, next_item)

        if not self._has_next_page:
            raise StopIteration

        if self._next_page is not None:
            resp = self._next_page.result()
        else:
            resp = self._records._session._session.get(self._records._url, params=self._records._params)
        resp.raise_for_status()
        resp = resp.json()

        self._page = iter(resp['data'])

        # update url for fetching next page
        next_page = resp['_links']['next']
        if next_page is None:
            self._has_next_page = False
        else:
            session = self._records._session
            self._next_page = session.pool.submit(session._session.get, next_page['href'])

        next_item = next(self._page, None)
        if next_item is not None:
            return Record(self._records._session, next_item)
        else:
            raise StopIteration


class Record:
    TYPE_HANDLERS = {
        # s → requests.Session, i → field content
        'binary': lambda s, i: _tocco.Binary(s, i),
        'birthdate': lambda s, i: datetime.strptime(i, '%Y-%m-%d').date(),
        'createts': lambda s, i: datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').replace(tzinfo=timezone.utc),
        'date': lambda s, i: datetime.strptime(i, '%Y-%m-%d').date(),
        'datetime': lambda s, i: datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').replace(tzinfo=timezone.utc),
        'document': lambda s, i: _tocco.Binary(s, i),
        'duration': lambda s, i: timedelta(milliseconds=i),
        'login': lambda s, i: i['username'],
        'updatets': lambda s, i: datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').replace(tzinfo=timezone.utc),
        'uuid': lambda s, i: uuid.UUID(i),
        'url': lambda s, i: i if i != '' else None
    }

    def __init__(self, session, raw):
        self._session = session
        self.raw = raw

    def __repr__(self):
        return '<record entity={!r} pk={!r}>'.format(self.model(), self.raw['key'])

    def model(self):
        return self.raw['model']

    @property
    def key(self):
        return self.raw['key']

    @property
    def fields(self):
        raw = self.raw.get('fields')

        if raw is None:
            return {}

        return { k: self._convert(v) for k, v in raw.items() }

    @functools.lru_cache()
    def get_field(self, name):
        self._convert(self.raw['fields'][name])

    @property
    def paths(self):
        # print('raw:', self.raw)
        raw = self.raw.get('paths')

        if raw is None:
            return {}

        return { k: self._convert_path_item(v) for k, v in raw.items() }

    def get_path(self, name):
        return self._convert_path_item(self.raw['paths'][name])

    def _convert_path_item(self, value):
        ftype = value['type']
        if ftype == 'field':
             return self._convert(value['value'])
        elif ftype == 'entity':
            return Record(self._session, value['value'])
        elif ftype == 'multi':
            return [self._convert(i['value']) for i in value['value']]
        else:
            raise AssertionError('Unknonw field type {!r}'.format(ftype))

    @functools.lru_cache()
    def get_relations(self, name):
        self._convert(self.raw['paths'][name]['value'])

    def _convert(self, field):
        value = field['value']
        # print('type:', type(value))
        if value is not None:
            handler = self.TYPE_HANDLERS.get(field['type'], lambda s, i: i)
            value = handler(self._session._session, value)

        # print(value)

        assert field['type'] in self.TYPE_HANDLERS \
                 or field['type'] in ['string', 'integer', 'email', 'counter', 'phone', 'decimal', 'text',
                                      'postcode', 'float', 'moneyamount', 'identifier', 'boolean', 'int',
                                      'sorting', 'long', 'time', 'html', 'updateuser', 'percent', 'serial'], \
               '{} - {} - {}'.format(field['type'], type(value), value)
        return value
