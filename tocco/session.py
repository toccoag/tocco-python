from concurrent import futures
import requests
import tocco as _tocco

class Session:
    def __init__(self, url, username, password):
        self.pool = None
        self.pool = futures.ThreadPoolExecutor(1)
        self._username = username
        self._url = url
        self._session = requests.Session()
        self._session.headers['Content-Type'] = 'application/json'
        self._session.auth = (username, password)
        self._test_login()
        self.pool = futures.ThreadPoolExecutor(1)

    def __enter__(self):
        return self

    def __exit__(self, a, b, c):
        if self.pool is not None:
            self.pool.shutdown()

    def _test_login(self):
        resp = self._session.get('{}/nice2/username'.format(self._url))
        resp.raise_for_status()
        resp = resp.json()
        username = resp['username']
        if username != self._username:
            raise AssertionError('Unexpected username {}'.format(username))

    def get_record(self, entity, pk, *, paths=None, fields=None, relations=None):
        resp = self._session.get( '{}/nice2/rest/entity/{}/{}'.format(self._url, self._entity, pk)).json()
        return resp

    def get_records(self, entity, *paths, where=None, fields=None, relations=None, offset=None, page_size=None, sort=None):
        url = '{}/nice2/rest/entities/{}'.format(self._url, entity)
        params = {}
        if where is not None:
            params['_where'] = where
        if fields is not None:
            if isinstance(fields, str):
                params['_fields'] = fields
            else:
                params['_fields'] = ','.join(fields)
        if relations is not None:
            if isinstance(relations, str):
                params['_relations'] = relations
            else:
                params['_relations'] = ','.join(relations)
        if offset is not None:
            params['_offset'] = offset
        if page_size is not None:
            params['_limit'] = page_size
        if sort is not None:
            params['_sort'] = sort
        if len(paths) > 0:
            params['_paths'] = ','.join(paths)
        return _tocco.Records(self, entity, url, params)
