from setuptools import setup

setup(name='tocco',
      version='0.0.1',
      description='High-level Tocco REST library',
      url='https://gitlab.com/toccoag/tocco-python',
      author='Peter Gerber',
      author_email='peter@arbitrary.ch',
      license='LGPL3',
      packages=['tocco'],
      zip_safe=False)
